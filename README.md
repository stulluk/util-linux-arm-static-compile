# util-linux-arm-static-compile

These scripts are used to statically compile util-linux ( dmesg, fdisk...etc ) for arm.

# Prequisities

```
sudo apt install gcc-arm-linux-gnueabi g++-arm-linux-gnueabi --install-recommends
```

```
wget https://mirrors.edge.kernel.org/pub/linux/utils/util-linux/v2.33/util-linux-2.33.tar.gz
tar xf util-linux-2.33.tar.gz
```