#!/bin/bash

set -e
#set -x

mkdir -p OUTPUT

cd util-linux-2.33

for i in *
do
	# The file is executable and not a directory
	if [ -x "$i" ] && [ ! -d "$i" ]
	then
		printf "The File $i is executable and not a directory\n"
		rsync -aPv $i ../OUTPUT/
	fi

done
